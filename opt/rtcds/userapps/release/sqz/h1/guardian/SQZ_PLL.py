# Nutsinee Kijbunchoo Sep 28, 2018
#
# -*- mode: python; tab-width: 4; indent-tabs-mode: nil -*-
#
# $Id: SQZ_PLL.py $
import sys
import time
from guardian import GuardState, GuardStateDecorator, NodeManager
import cdsutils as cdu

nominal = 'CHECK_BEATNOTE_2'

#############################################
#Function

#this fucntion return True when OPO locked on CLF DUAL (index = 17)
def OPO_LOCKED():
    log('Checking OPO in OPO_LOCKED()')
    flag = False
    if ezca['GRD-SQZ_OPO_STATE_N'] == 17:
        flag = True
    return flag

def IMC_LOCKED():
    log('Checking IMC in IMC_LOCKED()')
    flag = False
    if ezca['GRD-IMC_LOCK_STATE_N'] == 100 or ezca['GRD-IMC_LOCK_STATE_N'] == 70:
        flag = True
    return flag


#############################################
class INIT(GuardState):
    index = 0

    def run(self):
        return True # given the right edges this should go to DOWN

class DOWN(GuardState):
    index = 1
    goto = True

    def run(self):
        return True

class READY(GuardState):
    index = 2

    def run(self):
        if OPO_LOCKED() and IMC_LOCKED():
            return True 
        if not OPO_LOCKED():
            notify('Waiting for OPO LOCKED CLF DUAL')
        if not IMC_LOCKED():
            notify('Waiting for IMC LOCKED') 

class CHECK_BEATNOTE_2(GuardState):
    index = 4

    def main(self):
        self.delta_t = 1/16.
        self.ugf = 0.3
        self.cal = 7e-8  #Volt per Hz OPO PZT
        #self.counter = 0
        log('main of CHECK_BEATNOTE_2')
    
    def run(self):
        if not OPO_LOCKED():
            return 'IDLE'
        if not IMC_LOCKED():
            return 'IDLE'
        
        #self.counter += 1
        #log('Counter = {}'.format(self.counter))

        err = ezca['SQZ-FREQ_SQZBEATVSDOUBLEPSLVCO'] #ezca['SQZ-FREQ_SQZBEAT'] - 2*ezca['SQZ-FREQ_PSLVCO']
        self.cntrl = self.delta_t*self.ugf*err
        log('err = {}'.format(err))
        ezca['SQZ-OPO_PZT_1_OFFSET'] += self.cal*self.cntrl

        return True

class IDLE(GuardState):
    index = 7
    request = False

    def run(self):
        if not OPO_LOCKED() or not IMC_LOCKED():
            notify('OPO or IMC unlocked')
        else:
            return 'CHECK_BEATNOTE_2'

'''
        if not IMC_LOCKED():
            notify('IMC unlocked')
        else:
            return 'CHECK_BEATNOTE_2'
'''
           

#############################################

edges = [
    ('INIT', 'DOWN'),
    ('DOWN','READY'),
    ('READY', 'CHECK_BEATNOTE_2')   
]


